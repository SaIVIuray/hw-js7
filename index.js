function filterBy(arr, dataType) {
    return arr.filter(item => typeof item !== dataType);
  }
  
  const inputArray = ['Jack', 'Pokhyl', 28, '1995', null];
  const dataTypeToFilter = 'string';
  
  const filteredArray = filterBy(inputArray, dataTypeToFilter);
  console.log(filteredArray); 
  